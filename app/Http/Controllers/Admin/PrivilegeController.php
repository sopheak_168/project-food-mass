<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PrivilegeController extends Controller
{
    public function Index(){
        return View('admin.privilege.index');
    }
    public function create(){
        return View('admin.privilege.create');
    }
    public function store(){
        return redirect('/system/privilege');
    }
}
