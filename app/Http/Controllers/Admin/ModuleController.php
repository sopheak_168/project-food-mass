<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ModuleController extends Controller
{
    public function Index(){
        return View('admin.module.index');
    }
    public function create(){
        return View('admin.module.create');
    }
    public function store(){
        return redirect('/system/module');
    }
}
