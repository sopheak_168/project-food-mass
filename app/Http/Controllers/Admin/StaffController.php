<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class StaffController extends Controller
{
    public function Index(){
        return View('admin.staff.index');
    }
    public function create(){
        return View('admin.staff.create');
    }
    public function store(){
        return redirect('/system/staff');
    }
}
