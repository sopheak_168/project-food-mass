<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    public function Index(){
        return View('admin.role.index');
    }
    public function create(){
        return View('admin.role.create');
    }
    public function store(){
        return redirect('/system/role');
    }
}
