<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function Index()
    {
        return View('admin.user.index');
    }
    public function create()
    {
        return View('admin.user.create');
    }
    public function store()
    {
        return redirect('/system/user');
    }
}
