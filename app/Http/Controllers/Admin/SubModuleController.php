<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SubModuleController extends Controller
{
    public function Index(){
        return View('admin.submodule.index');
    }
    public function create(){
        return View('admin.submodule.create');
    }
    public function store(){
        return redirect('/system/submodule');
    }
}
