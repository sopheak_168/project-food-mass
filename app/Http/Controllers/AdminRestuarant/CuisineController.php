<?php

namespace App\Http\Controllers\AdminRestuarant;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CuisineController extends Controller
{
    public function Index()
    {
        return View('adminrestuarant.cuisine.index');
    }
    public function create()
    {
        return View('adminrestuarant.cuisine.create');
    }
    public function store()
    {
        return redirect('/systemres/cuisine');
    }
}
