<?php

namespace App\Http\Controllers\AdminRestuarant;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ResUserController extends Controller
{
    public function Index()
    {
        return View('adminrestuarant.resuser.index');
    }
    public function create()
    {
        return View('adminrestuarant.resuser.create');
    }
    public function store()
    {
        return redirect('/systemres/resuser');
    }
}
