<?php

namespace App\Http\Controllers\AdminRestuarant;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class OptionController extends Controller
{
    public function Index()
    {
        return View('adminrestuarant.option.index');
    }
    public function create()
    {
        return View('adminrestuarant.option.create');
    }
    public function store()
    {
        return redirect('/systemres/option');
    }
}
