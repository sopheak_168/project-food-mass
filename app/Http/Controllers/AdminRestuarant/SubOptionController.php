<?php

namespace App\Http\Controllers\AdminRestuarant;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SubOptionController extends Controller
{
    public function Index()
    {
        return View('adminrestuarant.suboption.index');
    }
    public function create()
    {
        return View('adminrestuarant.suboption.create');
    }
    public function store()
    {
        return redirect('/systemres/suboption');
    }
}
