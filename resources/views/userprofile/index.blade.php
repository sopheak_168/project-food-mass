@extends('master') @section('title','Userprofile') @section('content')
<!-- body userprofile -->
<div class="container">
    <div class="row">
        <div class="col-md-9">
            <div class="profile-box-left">
                <div class="profile-menu">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs tabs-edit" role="tablist">
                        <li role="presentation" class="active"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Profile</a></li>
                        <li role="presentation"><a href="#addressbook" aria-controls="addressbook" role="tab" data-toggle="tab">Address Book</a></li>
                        <li role="presentation"><a href="#orderhistory" aria-controls="orderhistory" role="tab" data-toggle="tab">Order History</a></li>
                        <!-- <li role="presentation"><a href="my-point.html" aria-controls="mypoints" role="tab">My Points</a></li> -->
                    </ul>
                </div>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="profile">
                        <div class="profile-box-body">
                            <div class="row">
                                <form>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>First Name</label>
                                            <input type="text" class="form-control" placeholder="First Name">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Last Name</label>
                                            <input type="text" class="form-control" placeholder="Last Name">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Email Address</label>
                                            <input type="email" class="form-control" placeholder="Email Address">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Contact Phone</label>
                                            <input type="text" class="form-control" placeholder="Contact Phone">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Password</label>
                                            <input type="password" class="form-control" placeholder="Password">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Confirm Password</label>
                                            <input type="password" class="form-control" placeholder="Confirm Password">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-info">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="addressbook">
                        <div class="profile-box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal">Add New</button>
                                </div>
                                <!-- popup -->
                                <div class="modal fade" id="modal" tabindex="-1" role="dialog">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title">Add New Address/h4>
                                            </div>

                                            <div class="modal-body">
                                                <form role="form" action="" method="POST" enctype="multipart/form-data">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Address</label>
                                                                <input type="text" name="name" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>City</label>
                                                                <input type="text" name="name" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>state</label>
                                                                <input type="text" name="name" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Zip code</label>
                                                                <input type="text" name="name" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Location Name</label>
                                                                <input type="text" name="name" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Country</label>
                                                                <select class="form-control">
                                                                          <option>Cambodia</option>
                                                                      </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="box-footer">
                                                        <button type="submit" name="btn_edit_menu" class="btn btn-primary">Submit</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- /.modal -->
                                <!-- end popup -->

                                <div class="col-md-12">
                                    <table class="table">
                                        <thead class="thead-default">
                                            <tr>
                                                <th>Address</th>
                                                <th>Location Name</th>
                                                <th>Default</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Phnom Penh</td>
                                                <td>Prek leap</td>
                                                <td>Phnom Penh</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div role="tabpanel" class="tab-pane" id="orderhistory">
                        <div class="profile-box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <h5>Your Recent Order</h5>
                                </div>

                                <div class="col-md-12">
                                    <p style="color: red;">No order history</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="mypoints">My Points</div>
                </div>

            </div>
        </div>
        <div class="col-md-3">
            <div class="box-profile">
                <div class="box-profile-img">
                    <img src="{{ url('img/avatar_1879754555620348.jpg') }}" alt="..." class="img-circle" style="height: 160px; width: 160px;">
                </div>
                <div class="box-browes">
                    <a href="#">browse</a>
                </div>
                <div class="box-profile-title">
                    Update your profile picture 
                </div>
            </div>
        </div>
    </div>
</div>
<!-- //end body userprofile -->
@endsection