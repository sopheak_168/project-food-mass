<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="en">

<head>
    <title>@yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="keywords" content="Staple Food Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
	SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design" />
    <script type="application/x-javascript">
        addEventListener("load", function() {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <!-- Custom Theme files -->
    <link href="{{ url('css/bootstrap.css') }}" type="text/css" rel="stylesheet" media="all">
    <link href="{{ url('css/style.css') }}" type="text/css" rel="stylesheet" media="all">
    <link href="{{ url('css/tyra.css') }}" type="text/css" rel="stylesheet" media="all">
    <link href="{{ url('css/sopheak.css') }}" type="text/css" rel="stylesheet" media="all">
    <link href="{{ url('css/sopha.css') }}" type="text/css" rel="stylesheet" media="all">
    <link href="{{ url('css/set1.css') }}" type="text/css" rel="stylesheet" media="all">
    <link href="{{ url('css/font-awesome.css') }}" rel="stylesheet">
    <!-- font-awesome icons -->
    <link rel="stylesheet" href="{{ url('css/owl.carousel.css') }}" type="text/css" media="all" />
    <!-- Owl-Carousel-CSS -->
    <!-- //Custom Theme files -->
    <!-- web-fonts -->
    {{--
    <link href="//fonts.googleapis.com/css?family=Berkshire+Swash" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Yantramanav:100,300,400,500,700,900" rel="stylesheet"> --}}
    <!-- //web-fonts -->
</head>

<body>
    <!-- banner -->
    <div class="banner">
        <!-- header -->
        <div class="header">
            <div class="w3ls-header">
                <!-- header-one -->
                <div class="container">
                    <div class="w3ls-header-left">
                        {{--
                        <p>Free home delivery at your doorstep For Above $30</p> --}}
                    </div>
                    <div class="w3ls-header-right">
                        <ul>
                            <li class="head-dpdn">
                                <i class="fa fa-phone" aria-hidden="true"></i> Call us: +01 222 33345
                            </li>
                            <li class="head-dpdn">
                                <a href="{{url('/home/login') }}"><i class="fa fa-sign-in" aria-hidden="true"></i> Login & Signup</a>
                            </li>
                            <li class="head-dpdn">
                                <a href="{{ url('/home/userprofile') }}"><i class="fa fa-user" aria-hidden="true"></i> User-Profile</a>
                            </li>
                             <li class="head-dpdn">
                                <a href="{{ url('/home/registerrest') }}"><i class="fa fa-cutlery" aria-hidden="true"></i> Restuarant Register</a>
                            </li>
                            <li class="head-dpdn">
                                <a href="#"><i class="fa fa-question-circle" aria-hidden="true"></i> Help</a>
                            </li>
                        </ul>
                    </div>
                    <div class="clearfix"> </div>
                </div>
            </div>
            <!-- //header-one -->
            <!-- navigation -->
            <div class="navigation agiletop-nav">
                <div class="container">
                    <nav class="navbar navbar-default">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header w3l_logo">
                            <button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
                            <h1><a href="{{ url('/home') }}">Staple<span>Best Food Collection</span></a></h1>
                        </div>
                        <div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="{{ url('/home') }}">Home</a></li>
                                <li><a href="{{ url('/home/browse') }}">BROW RESTAURANTS</a></li>
                                <!-- Mega Menu -->
                                <li><a href="{{ url('/home/contact') }}">Contact Us</a></li>
                            </ul>
                        </div>
                        <div class="cart cart box_1">
                            <button class="w3view-cart" data-toggle="modal" data-target="#addcart"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i></button>
                        </div>
                    </nav>
                </div>
            </div>
            <!-- //navigation -->
        </div>
        <!-- //header-end -->

        <!-- popup -->
        <div class="modal fade modal-defualt" id="addcart" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content" style="    margin-top: 77px;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Shopping Cart</h4>
                    </div>
                    <div class="modal-footer" style="text-align: left !important;">
                        <form action="" method="POST" enctype="multipart/form-data">
                            {{-- {{ csrf_field() }} --}}
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12 col-ms-12 col-xs-12">
                                        <div class="" style="overflow: scroll; 
            overflow-x: hidden;
            height: 299px;">
                                            <table class="table table-condensed">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Name</th>
                                                        <th>Prices</th>
                                                        <th> Qty</th>
                                                        <th>Action</th>
                                                    </tr>

                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td> 1</td>
                                                        <td> Coca Cola</td>
                                                        <td> 100$</td>
                                                        <td> 19</td>
                                                        <td> New</td>
                                                    </tr>
                                                    <tr>
                                                        <td> 1</td>
                                                        <td> Coca Cola</td>
                                                        <td> 100$</td>
                                                        <td> 19</td>
                                                        <td> New</td>
                                                    </tr>
                                                    <tr>
                                                        <td> 1</td>
                                                        <td> Coca Cola</td>
                                                        <td> 100$</td>
                                                        <td> 19</td>
                                                        <td> New</td>
                                                    </tr>
                                                    <tr>
                                                        <td> 1</td>
                                                        <td> Coca Cola</td>
                                                        <td> 100$</td>
                                                        <td> 19</td>
                                                        <td> New</td>
                                                    </tr>
                                                    <tr>
                                                        <td> 1</td>
                                                        <td> Coca Cola</td>
                                                        <td> 100$</td>
                                                        <td> 19</td>
                                                        <td> New</td>
                                                    </tr>

                                                    <tr>
                                                        <td> 1</td>
                                                        <td> Coca Cola</td>
                                                        <td> 100$</td>
                                                        <td> 19</td>
                                                        <td> New</td>
                                                    </tr>
                                                    <tr>
                                                        <td> 1</td>
                                                        <td> Coca Cola</td>
                                                        <td> 100$</td>
                                                        <td> 19</td>
                                                        <td> New</td>
                                                    </tr>
                                                    <tr>
                                                        <td> 1</td>
                                                        <td> Coca Cola</td>
                                                        <td> 100$</td>
                                                        <td> 19</td>
                                                        <td> New</td>
                                                    </tr>
                                                    <tr>
                                                        <td> 1</td>
                                                        <td> Coca Cola</td>
                                                        <td> 100$</td>
                                                        <td> 19</td>
                                                        <td> New</td>
                                                    </tr>
                                                    <tr>
                                                        <td> 1</td>
                                                        <td> Coca Cola</td>
                                                        <td> 100$</td>
                                                        <td> 19</td>
                                                        <td> New</td>
                                                    </tr>
                                                    <tr>
                                                        <td> 1</td>
                                                        <td> Coca Cola</td>
                                                        <td> 100$</td>
                                                        <td> 19</td>
                                                        <td> New</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="pull-right">
                                            {{-- <input style="margin-top: 10px; background: #5e48d4; color:white;" class="btn btn-default" type="submit" value="Procces to check out" style=""> --}}
                                            <a href="{{ url('home/payment') }}" style="margin-top: 10px; background: #5e48d4; color:white;" class="btn btn-default">Procces to check out</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- endpopup -->

        <!-- banner-text -->
        <div class="banner-text">
            <div class="container">
                <h2>Delicious food from the <br> <span>Best Chefs For you.</span></h2>
                <div class="agileits_search">
                    <form action="#" method="post">
                        <input name="Search" type="text" placeholder="Enter Your Area Name" required="">
                        <select id="agileinfo_search" name="agileinfo_search" required="">
							<option value="">Popular Cities</option>
							<option value="navs">New York</option>
							<option value="quotes">Los Angeles</option>
							<option value="videos">Chicago</option>
							<option value="news">Phoenix</option>
							<option value="notices">Fort Worth</option>
							<option value="all">Other</option>
						</select>
                        <input type="submit" value="Search">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- //banner -->

    <!-- container -->
    <div class="w3agile-spldishes">
        @yield('content')
    </div>
    <!-- //container -->

    <!-- footer -->
    <div class="footer agileits-w3layouts">
        <div class="container">
            <div class="w3_footer_grids">
                <div class="col-xs-6 col-sm-3 footer-grids w3-agileits">
                    <h3>company</h3>
                    <ul>
                        <li><a href="about.html">About Us</a></li>
                        <li><a href="contact.html">Contact Us</a></li>
                        <li><a href="careers.html">Careers</a></li>
                        <li><a href="help.html">Partner With Us</a></li>
                    </ul>
                </div>
                <div class="col-xs-6 col-sm-3 footer-grids w3-agileits">
                    <h3>help</h3>
                    <ul>
                        <li><a href="faq.html">FAQ</a></li>
                        <li><a href="login.html">Returns</a></li>
                        <li><a href="login.html">Order Status</a></li>
                        <li><a href="offers.html">Offers</a></li>
                    </ul>
                </div>
                <div class="col-xs-6 col-sm-3 footer-grids w3-agileits">
                    <h3>policy info</h3>
                    <ul>
                        <li><a href="terms.html">Terms & Conditions</a></li>
                        <li><a href="privacy.html">Privacy Policy</a></li>
                        <li><a href="login.html">Return Policy</a></li>
                    </ul>
                </div>
                <div class="col-xs-6 col-sm-3 footer-grids w3-agileits">
                    <h3>Menu</h3>
                    <ul>
                        <li><a href="menu.html">All Day Menu</a></li>
                        <li><a href="menu.html">Lunch</a></li>
                        <li><a href="menu.html">Dinner</a></li>
                        <li><a href="menu.html">Flavours</a></li>
                    </ul>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>

    </div>
    <div class="copyw3-agile">
        <div class="container">
            <p>&copy; 2017 Staple Food. All rights reserved | Design by <a href="http://w3layouts.com/">W3layouts</a></p>
        </div>
    </div>
    <!-- //footer -->
    <!-- cart-js -->
    <script src="{{ url('js/minicart.js') }}"></script>

    <!-- js -->
    <script src="{{ url('js/jquery-2.2.3.min.js') }}"></script>
    <!-- //js -->
    <script>
        w3ls.render();

        w3ls.cart.on('w3sb_checkout', function(evt) {
            var items, len, i;

            if (this.subtotal() > 0) {
                items = this.items();

                for (i = 0, len = items.length; i < len; i++) {}
            }
        });
    </script>
    <!-- //cart-js -->
    <!-- Owl-Carousel-JavaScript -->
    <script src="{{ url('js/owl.carousel.js')}}"></script>
    <script>
        $(document).ready(function() {
            $("#owl-demo").owlCarousel({
                items: 3,
                lazyLoad: true,
                autoPlay: true,
                pagination: true,
            });
        });
    </script>
    <!-- //Owl-Carousel-JavaScript -->
    <!-- start-smooth-scrolling -->
    <script src="{{ url('js/SmoothScroll.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/move-top.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/easing.js') }}"></script>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $(".scroll").click(function(event) {
                event.preventDefault();

                $('html,body').animate({
                    scrollTop: $(this.hash).offset().top
                }, 1000);
            });
        });
    </script>
    <!-- //end-smooth-scrolling -->
    <!-- smooth-scrolling-of-move-up -->
    <script type="text/javascript">
        $(document).ready(function() {
            /*
            var defaults = {
            	containerID: 'toTop', // fading element id
            	containerHoverID: 'toTopHover', // fading element hover id
            	scrollSpeed: 1200,
            	easingType: 'linear' 
            };
            */

            $().UItoTop({
                easingType: 'easeOutQuart'
            });

        });
        $(document).ready(function() {

            var quantitiy = 0;
            $('.quantity-right-plus').click(function(e) {

                // Stop acting like a button
                e.preventDefault();
                // Get the field name
                var quantity = parseInt($('#quantity').val());

                // If is not undefined

                $('#quantity').val(quantity + 1);


                // Increment

            });

            $('.quantity-left-minus').click(function(e) {
                // Stop acting like a button
                e.preventDefault();
                // Get the field name
                var quantity = parseInt($('#quantity').val());

                // If is not undefined

                // Increment
                if (quantity > 0) {
                    $('#quantity').val(quantity - 1);
                }
            });

        });
    </script>
    <!-- //smooth-scrolling-of-move-up -->
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="{{ url('js/bootstrap.js') }}"></script>
    <a href="#" id="toTop" style="display: inline;"><span id="toTopHover" style="opacity: 0;"></span>To Top</a> {{-- <a href="#" id="toTop" style="display: block;"><span id="toTopHover" style="opacity: 0;"></span>To Top</a> --}}

    <!-- My Order Pop up  -->
    <div class="modal fade modal-defualt" id="myDetail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="    margin-top: 150px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Shopping Cart</h4>
                </div>
                <div class="modal-footer" style="text-align: left !important;">
                    <form action="#" method="POST" enctype="multipart/form-data">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12 col-ms-12 col-xs-12">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="box-popup-price">
                                                <b>Price :</b> 9 g $ 1.25
                                            </div>
                                            <div class="box-popup-price2">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="box-popup-price">
                                                <b>Quantity :</b>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="box-popup-quantity">
                                                <div class="input-group">
                                                    <span class="input-group-btn">
                                        <button type="button" class="quantity-left-minus btn btn-danger btn-number  no-radius"  data-type="minus" data-field="">
                                          <span class="glyphicon glyphicon-minus"></span>
                                                    </button>
                                                    </span>
                                                    <input type="text" id="quantity" name="quantity" class="form-control input-number nop" value="1" min="1" max="100">

                                                    <span class="input-group-btn">
                                        <button type="button" class="quantity-right-plus btn btn-success btn-number no-radius" data-type="plus" data-field="">
                                            <span class="glyphicon glyphicon-plus"></span>
                                                    </button>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3"></div>
                                    </div>
                                    <!-- </div> -->
                                    <div class="pull-right">
                                        {{-- <input style="margin-top: 10px; background: red; color:white;" class="btn btn-default" type="submit" value="Close"> --}}
                                        <a href="{{ url('/home/menu') }}" style="margin-top: 10px; background: red; color:white;" class="btn btn-default">Close</a>
                                    </div>
                                    <div class="pull-right">
                                        {{-- <input style="margin-top: 10px; background: #5e48d4; color:white;" class="btn btn-default" type="submit" value="Add to Card"> --}}
                                        <a href="{{ url('/home/menu') }}" style="margin-top: 10px; background: #5e48d4; color:white;" class="btn btn-default">Add to Card</a>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--End My Order-->
</body>

</html>