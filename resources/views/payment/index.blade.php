@extends("master") 
@section('title', 'Payment') 
@section('content')
<div class="container">
    <div class="row">

        <div class="col-md-12">
            <h3 class="w3ls-title">Payment</h3>
            <div class="hid-space"></div>
        </div>

        <div class="col-sm-5">
            <div class="payment-left">
                <h4 class="w3ls-title w3ls-title1">Order Summery</h4>
                <div class="or-list-title">
                    <h4 class="title-list-or">MCDONALDS</h4>
                </div>
                <table class="table">
                    <thead class="thead-default">
                        <tr>
                            <th>Item</th>
                            <th>Qty</th>
                            <th>Rate</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Mark</td>
                            <td>1</td>
                            <td>12$</td>
                        </tr>
                        <tr>
                            <td>Mark</td>
                            <td>1</td>
                            <td>12$</td>
                        </tr>
                        <tr>
                            <td>Mark</td>
                            <td>1</td>
                            <td>12$</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td style="color: rgba(253, 70, 62, 0.84);">
                                <h5>Total</h5>
                            </td>
                            <td>36$</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="col-md-7">
            <div class="title-center-form">
                <h3>ORDER DETAIL</h3>
                <h4>PICKUP DETAIL</h4>
            </div>
            <div class="line-title-form"></div>
            <div class="form-payment">
                <form class="form-horizontal">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-md-3 control-label">Name</label>
                        <div class="col-md-9">
                            <input type="email" class="form-control" id="inputEmail3" placeholder="Name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-md-3 control-label">E-mail</label>
                        <div class="col-md-9">
                            <input type="email" class="form-control" id="inputEmail3" placeholder="E-mail">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-md-3 control-label">Full Address</label>
                        <div class="col-md-9">
                            <input type="email" class="form-control" id="inputEmail3" placeholder="Full Address">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-md-3 control-label">Phone</label>
                        <div class="col-md-9">
                            <input type="email" class="form-control" id="inputEmail3" placeholder="Phone">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-md-3 control-label">Discount coupon</label>
                        <div class="col-md-9">
                            <input type="email" class="form-control" id="inputEmail3" placeholder="Discount coupon">
                        </div>
                    </div>

                    <div class="form-group space-top-in-form">
                        <label for="inputEmail3" class="col-md-3 control-label">Pickup Date</label>
                        <div class="col-md-9">
                            <select class="form-control">
                                        <option>A</option>
                                        <option>B</option>
                                        <option>C</option>
                                        <option>D</option>
                                    </select>
                        </div>
                    </div>
                </form>
            </div>
            <div class="title-center-form">
                <h3>PAYMENT METHOD</h3>
            </div>
            <div class="hid-space"></div>
            <div class="row">
                <div class="col-md-3">
                    <div class="radio">
                        <input type="radio" name="radio1" id="radio1" value="option1" checked="">
                        <label for="radio1">
                                        Cash on Delivery
                                    </label>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="radio">
                        <input type="radio" name="radio1" id="radio2" value="option2">
                        <label for="radio2">
                                        Pay with SmartLuy
                                    </label>
                    </div>
                </div>
                <div class="col-md-3">
                    <!-- <div style="background: gray; height: 50px;"></div> -->
                </div>
                <div class="col-md-3">
                    <!-- <div style="background: gray; height: 50px;"></div> -->
                </div>
            </div>
            <div class="hid-space"></div>
            <div class="row">
                <div class="col-md-12">
                    <button type="button" class="btn btn-danger">Back</button>
                    <button type="button" class="btn btn-danger">Order Now</button>
                </div>
            </div>
        </div>

    </div>
</div>
<!--end section-white-->

<div class="hid-space"></div>
@endsection