@extends('admin.master')

@section('title','Module')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-user"></i> Add Module</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <form role="form" action="{{ url('/system/module') }}" method="POST">
                        {{ csrf_field() }}
                        <!-- text input -->
                        <div class="form-group">
                            <label>Module ID</label>
                            <input type="text" name="" class="form-control" placeholder="Enter your Module ID">
                        </div>
                        <div class="form-group">
                            <label>Module Name</label>
                            <input type="text" name="" class="form-control" placeholder="Enter your Module name">
                        </div>
                        <div class="box-footer">
                            <input type="submit" value="Save" class="btn btn-primary pull-right">
                        </div>
                    </form>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
@endsection