@extends("admin.master") @section('title', 'User') @section('content')

<div class="row">
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-user"></i> Add User</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <form role="form" action="{{ url('/system/user') }}" method="POST">
                    {{ csrf_field() }}
                    <!-- text input -->
                    <div class="form-group">
                        <label>Role</label>
                        <select class="form-control" name="role">
                            <option value="admin">Admin</option>
                            <option value="reporter">Reporter</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Staff ID</label>
                        <input type="text" name="" class="form-control" placeholder="Enter your Staff ID">
                    </div>
                    <div class="form-group">
                        <label>Username</label>
                        <input type="text" name="" class="form-control" placeholder="Enter your Username">
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" name="" class="form-control" placeholder="Enter your password">
                    </div>
                    <div class="form-group">
                        <label>Confirm Password</label>
                        <input type="password" name="" class="form-control" placeholder="Enter your confirm password">
                    </div>
                    <div class="box-footer">
                        <input type="submit" value="Save" class="btn btn-primary pull-right">
                    </div>
                </form>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
</div>

@endsection