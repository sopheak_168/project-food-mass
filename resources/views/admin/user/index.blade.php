@extends('admin.master') 

@section('title','User')

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title"><i class="fa fa-users"></i>Users</h3>
            <a class="btn btn-primary pull-right" href="{{ url('/system/user/create') }}" role="button">Create New User</a>
        </div>
        <div class="box-body">
            <table class="table table-bordered" id="app-table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Staff ID</th>
                        <th>Username</th>
                        <th>Role</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>S001</td>
                        <td>Sopha</td>
                        <td>Admin</td>
                        <td>
                            <a href="#"><i class="fa fa-lock"></i></a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection