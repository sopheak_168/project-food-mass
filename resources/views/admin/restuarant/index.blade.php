@extends('admin.master')

@section('title','Restuarant')

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title"><i class="fa fa-users"></i> Restuarant</h3>
            <a class="btn btn-primary pull-right" href="{{ url('/system/restuarant/create') }}" role="button">Create New Restuarant</a>
        </div>
        <div class="box-body">
            <table class="table table-bordered" id="app-table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>ResID</th>
                        <th>ResName</th>
                        <th>Address</th>
                        <th>ResTel</th>
                        <th>ResEmail</th>
                        {{--  <th>ConName</th>  --}}
                        {{--  <th>Position</th>  --}}
                        {{--  <th>ConTel</th>  --}}
                        {{--  <th>ConEmail</th>  --}}
                        {{--  <th>Open&closeTime</th>  --}}
                        {{--  <th>StatusA</th>
                        <th>StatusB</th>  --}}
                        <th>RegisterDate</th>
                        <th>SID</th>
                        <th>CoisineID</th>
                        <th>Link</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>R001</td>
                        <td>Amazom</td>
                        <td>PP</td>
                        <td>XXX</td>
                        <td>a@gmail.com</td>
                        {{--  <td>A</td>  --}}
                        {{--  <td>Test</td>  --}}
                        {{--  <td>XXX</td>  --}}
                        {{--  <td>a@gmail.com</td>  --}}
                        {{--  <td>7am-8pm</td>  --}}
                        {{--  <td>Open</td>
                        <td>Test</td>  --}}
                        <td>12/08/2017</td>
                        <td>S001</td>
                        <td>C002</td>
                        <td>
                            <a href="{{ url('/systemres/dashboard') }}"><i class="fa fa-link">Link</i></a>
                        </td>
                        <td>
                            <a href="#"><i class="fa fa-lock"></i></a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection