@extends('admin.master')

@section('title','Privilege')

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title"><i class="fa fa-users"></i> Privilege</h3>
            <a class="btn btn-primary pull-right" href="{{ url('/system/privilege/create') }}" role="button">Create New Privilege</a>
        </div>
        <div class="box-body">
            <table class="table table-bordered" id="app-table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Role ID</th>
                        <th>Module ID</th>
                        <th>SunModule ID</th>
                        <th>Privilege</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>R001</td>
                        <td>M001</td>
                        <td>SM001</td>
                        <td>Test</td>
                        <td>
                            <a href="#"><i class="fa fa-lock"></i></a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection