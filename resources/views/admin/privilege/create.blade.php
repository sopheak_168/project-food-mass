@extends("admin.master") @section('title', 'Privilege') @section('content')

<div class="row">
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-user"></i> Add Privilege</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <form role="form" action="{{ url('/system/privilege') }}" method="POST">
                    {{ csrf_field() }}
                    <!-- text input -->
                    <div class="form-group">
                        <label>Role</label>
                        <select class="form-control" name="role">
                            <option value="">--select--</option>
                            <option value="">test1</option>
                            <option value="">test2</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Module</label>
                        <select class="form-control" name="role">
                            <option value="">--select--</option>
                            <option value="">test1</option>
                            <option value="">test2</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>SubModule</label>
                        <select class="form-control" name="role">
                            <option value="">--select--</option>
                            <option value="">test1</option>
                            <option value="">test2</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Privilege</label>
                        <input type="text" name="" class="form-control" placeholder="Enter your Privilege">
                    </div>
                    <div class="box-footer">
                        <input type="submit" value="Save" class="btn btn-primary pull-right">
                    </div>
                </form>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
</div>

@endsection