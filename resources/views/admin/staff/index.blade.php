@extends('admin.master') 

@section('title','Staff')

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title"><i class="fa fa-users"></i>Staff</h3>
            <a class="btn btn-primary pull-right" href="{{ url('/system/staff/create') }}" role="button">Create New Staff</a>
        </div>
        <div class="box-body">
            <table class="table table-bordered" id="app-table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Staff ID</th>
                        <th>Staff Name</th>
                        <th>Gender</th>
                        <th>DOB</th>
                        <th>Address</th>
                        <th>Tel</th>
                        <th>Email</th>
                        <th>Salary</th>
                        <th>Position</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>S001</td>
                        <td>Sopha</td>
                        <td>Male</td>
                        <td>14/03/1997</td>
                        <td>Phnom Phenh</td>
                        <td>070207484</td>
                        <td>sopha@gmail.com</td>
                        <td>500$</td>
                        <td>IT</td>
                        <td>
                            <a href="#"><i class="fa fa-lock"></i></a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection