@extends('master') @section('title','Login') @section('content')
<div class="container">

    <div class="row">
        <div class="col-md-12">
            <h3 class="w3ls-title">Form Login</h3>
            <div class="hid-space"></div>
        </div>
        <div class="box-cover-form">

            <div class="col-sm-6 col-md-6">
                <div class=" border-style ">
                    <div class="sign-up-icon">
                        <div class="hid-space"></div>
                        <h2><i class="fa fa-sign-in" aria-hidden="true" style="float:left;"></i></h2>
                        <h4 class="h4-style-login">Log in</h4>
                        <div class="hid-space"></div>
                    </div>
                    <form>

                        <div class="row">
                            <div class="col-sm-12 col-md-12" style="margin-bottom: 20px; ">
                                <a href="#"> <input type="submit" value="Connect with facebook" class="btn btn-primary" style="font-size: 15px; width: 70%; margin-left: 35px;"></a>
                                <!-- <button type="button" class="btn btn-warning">Create Account</button> -->
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12 col-md-12" style="margin-bottom: 20px; ">
                                <a href="#"> <input type="submit" value="Connect with Gmail" class="btn btn-danger" style="font-size: 15px; width: 70%; margin-left: 35px;"></a>
                                <!-- <button type="button" class="btn btn-warning">Create Account</button> -->
                            </div>
                        </div>


                        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Email">


                        <input type="password" class="form-control" id="exampleInputEmail1" placeholder="password">

                        <div class="row">
                            <div class="col-sm-12 col-md-12" style="margin-bottom: 20px; ">
                                <!--<a href="#"> -->
                                <input type="submit" value="Log in" class="btn btn-success" style="font-size: 15px; width: 40%; margin-left: 35px; margin-top: 10px; float: right;">
                                <!--</a>-->

                            </div>
                        </div>

                    </form>
                </div>
                <!--close border-style-->
            </div>

            <!--sign up form-->
            <div class="col-sm-6 col-md-6">
                <div class=" border-style ">
                    <div class="sign-up-icon">
                        <div class="hid-space"></div>
                        <h2><i class="fa fa-sign-in" aria-hidden="true" style="float:left;"></i></h2>
                        <h4 class="h4-style-login">Sign up</h4>
                        <div class="hid-space"></div>
                    </div>
                    <form>

                        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="First Name">
                        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Last Name">
                        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Country & +855">
                        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Email Address">
                        <input type="password" class="form-control" id="exampleInputEmail1" placeholder="Password">
                        <input type="password" class="form-control" id="exampleInputEmail1" placeholder="Comfirm Password" style="margin-bottom: 5px;">
                        <p>By creating an account, you agree to receive sms from vendor.</p>

                        <div class="row">
                            <div class="col-sm-1 col-md-1 check">
                                <input type="checkbox">
                            </div>
                            <div class="col-sm-11 col-md-11 agree-condition">
                                <p>I Agree To <a href="#">The Terms & Conditions</a></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-12" style="margin-bottom: 20px; ">
                                <!--<a href="#">-->
                                <input type="submit" value="Create Account" class="btn btn-warning" style="font-size: 15px; width: 100%;">
                                <!--</a>-->

                            </div>
                        </div>


                    </form>
                </div>
                <!--close border-style-->
            </div>

        </div>
        <!--close box-cover-form-->
    </div>
</div>
@endsection