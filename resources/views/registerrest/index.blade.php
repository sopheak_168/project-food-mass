@extends('master')
@section('title','RegisterRest')
@section('content')
     <!-- container -->
    <div class="w3agile-spldishes">
        <div class="container">
            <div class="row">
                <div class="box-cover-form">
                    <!--sign up form-->
                    <div class="col-sm-12 col-md-12 border-style">
                        <div class="sign-up-icon">
                            <div class="hid-space"></div>
                            <h2><i class="fa fa-sign-in" aria-hidden="true" style="float:left;"></i></h2>
                            <h4 class="h4-style-login">Restaurants Register</h4>
                            <div class="hid-space"></div>
                        </div>

                        <div class="col-sm-4 col-md-4">

                            <form>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Restaurants' Name</label>
                                    <input type="text" class="form-control" placeholder="Restaurants' Name">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Address</label>
                                    <input type="text" class="form-control" placeholder="Address">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Restaurants Tel</label>
                                    <input type="text" class="form-control" placeholder="Email">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Restaurants Email</label>
                                    <input type="email" class="form-control" placeholder="Restaurants Email">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Contact Name</label>
                                    <input type="text" class="form-control" placeholder="Contact Name">
                                </div>

                            </form>
                        </div>

                        <div class="col-sm-4 col-md-4">
                            <form>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Position</label>
                                    <input type="text" class="form-control" placeholder="Position">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Contact Tel</label>
                                    <input type="text" class="form-control" placeholder="Phone number">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Contact Email</label>
                                    <input type="text" class="form-control" placeholder="Contact Email">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Open Time</label>
                                    <input type="time" class="form-control" placeholder="Open Time">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Close Time</label>
                                    <input type="time" class="form-control" placeholder="Close Time">
                                </div>

                            </form>
                        </div>

                        <div class="col-sm-4 col-md-4">
                            <form>

                                <!-- <label for="exampleInputEmail1">Open or Close Restaurant </label>
                                <div class="well well-sm" style="background-color: rgba(41, 40, 40, 0.02);">

                                    <label class="radio-inline">
                                                <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1"> Open
                                            </label>

                                    <label class="radio-inline">
                                                <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1"> Close
                                            </label>
                                </div> -->
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Register Date Time</label>
                                    <input type="datetime-local" class="form-control" placeholder="Register Date Time">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Staff Name</label>
                                    <input type="text" class="form-control" placeholder="Staff Name">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Cuisine Name</label>
                                    <select class="form-control">
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                          </select>
                                </div>
                                <p>By creating an account, you agree to receive sms from vendor.</p>

                                <div class="row">
                                    <div class="col-sm-1 col-md-1 check">
                                        <input type="checkbox">
                                    </div>
                                    <div class="col-sm-11 col-md-11 agree-condition">
                                        <p>I Agree To <a href="#">The Terms & Conditions</a></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 col-md-12" style="margin-bottom: 20px; ">
                                        <!--<a href="#">-->
                                        <input type="submit" value="Register" class="btn btn-warning" style="font-size: 15px; width: 100%; margin-top: 36px;">
                                        <!--</a>-->

                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>

                </div>
                <!--close box-cover-form-->
            </div>
        </div>
        <!-- //end body userprofile -->
    </div>
    <!-- //container -->
@endsection