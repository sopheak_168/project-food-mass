@extends('master')
@section('title','Contact')
@section('content')
    <!-- contact -->
    <div id="contact" class="contact cd-section">
        <div class="container">
            <h3 class="w3ls-title">Contact us</h3>
            <p class="w3lsorder-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit sheets containing sed </p>
            <div class="contact-row">
                <div class="col-xs-6 col-sm-6 contact-w3lsleft">
                    <div class="contact-grid agileits">
                        <h4>DROP US A LINE </h4>
                        <form action="#" method="post">
                            <input type="text" name="Name" placeholder="Name" required="">
                            <input type="email" name="Email" placeholder="Email" required="">
                            <input type="text" name="Phone Number" placeholder="Phone Number" required="">
                            <textarea name="Message" placeholder="Message..." required=""></textarea>
                            <input type="submit" value="Submit">
                        </form>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6">
                    <h6><span>Sed interdum </span>interdum accumsan nec purus ac orci finibus facilisis. In sit amet placerat nisl in auctor sapien. </h6>
                    <div class="address-row">
                        <div class="col-xs-2 address-left">
                            <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
                        </div>
                        <div class="col-xs-10 address-right">
                            <h5>Visit Us</h5>
                            <p>Broome St, Canada, NY 10002, New York </p>
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                    <div class="address-row w3-agileits">
                        <div class="col-xs-2 address-left">
                            <span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>
                        </div>
                        <div class="col-xs-10 address-right">
                            <h5>Mail Us</h5>
                            <p><a href="mailto:info@example.com"> mail@example.com</a></p>
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                    <div class="address-row">
                        <div class="col-xs-2 address-left">
                            <span class="glyphicon glyphicon-phone" aria-hidden="true"></span>
                        </div>
                        <div class="col-xs-10 address-right">
                            <h5>Call Us</h5>
                            <p>+01 222 333 4444</p>
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
    </div>
    <!-- //contact -->
@endsection