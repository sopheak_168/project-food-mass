<!-- Sidebar Menu -->
<ul class="sidebar-menu">
    <li class="header">HEADER</li>
    <!-- Optionally, you can add icons to the links -->
    <li class="active"><a href="{{ url('systemres/dashboard') }}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
    <li><a href="{{ url('systemres/menu') }}"><i class="fa fa-dashboard"></i> <span>Menu</span></a></li>
    <li><a href="{{ url('systemres/menutype') }}"><i class="fa fa-dashboard"></i> <span>MenuType</span></a></li>
    <li><a href="{{ url('systemres/varsize') }}"><i class="fa fa-dashboard"></i> <span>VarSize</span></a></li>
    <li><a href="{{ url('systemres/size') }}"><i class="fa fa-dashboard"></i> <span>Size</span></a></li>
    <li><a href="{{ url('systemres/varoption') }}"><i class="fa fa-dashboard"></i> <span>VarOption</span></a></li>
    <li><a href="{{ url('systemres/option') }}"><i class="fa fa-dashboard"></i> <span>Option</span></a></li>
    <li><a href="{{ url('systemres/suboption') }}"><i class="fa fa-dashboard"></i> <span>SubOption</span></a></li>
    <li><a href="{{ url('systemres/cuisine') }}"><i class="fa fa-dashboard"></i> <span>Cuisine</span></a></li>
    <li><a href="{{ url('systemres/resuser') }}"><i class="fa fa-dashboard"></i> <span>ResUser</span></a></li>

    {{--  <li class="treeview">
        <a href="#"><i class="fa fa-user"></i> <span>Link</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
        </a>
        <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i> Link</a></li>
        </ul>
    </li>  --}}

</ul>
<!-- /.sidebar-menu -->