@extends('adminrestuarant.master')

@section('title','Menu Restuarant')

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title"><i class="fa fa-users"></i> Menu Restuarant</h3>
            <a class="btn btn-primary pull-right" href="{{ url('/systemres/menu/create') }}" role="button">Create New Menu</a>
        </div>
        <div class="box-body">
            <table class="table table-bordered" id="app-table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Res ID</th>
                        <th>Menu ID</th>
                        <th>Menu Name</th>
                        <th>Menu TypeID</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>R001</td>
                        <td>M001</td>
                        <td>Test1</td>
                        <td>Test1</td>
                        <td>
                            <a href="#"><i class="fa fa-lock"></i></a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection