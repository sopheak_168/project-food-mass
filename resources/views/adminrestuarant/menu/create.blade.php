@extends('adminrestuarant.master')

@section('title','Menu Restuarant')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-user"></i> Add Menu</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <form role="form" action="{{ url('/systemres/menu') }}" method="POST">
                        {{ csrf_field() }}
                        <!-- text input -->
                        <div class="form-group">
                            <label>Res ID</label>
                            <input type="text" name="" class="form-control" placeholder="Enter your Res ID">
                        </div>
                        <div class="form-group">
                            <label>Menu ID</label>
                            <input type="text" name="" class="form-control" placeholder="Enter your Menu ID">
                        </div>
                        <div class="form-group">
                            <label>Menu Name</label>
                            <input type="text" name="" class="form-control" placeholder="Enter your Menu name">
                        </div>
                        <div class="form-group">
                            <label>Menu TypeID</label>
                            <input type="text" name="" class="form-control" placeholder="Enter your Menu TypeID">
                        </div>
                        <div class="box-footer">
                            <input type="submit" value="Save" class="btn btn-primary pull-right">
                        </div>
                    </form>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
@endsection