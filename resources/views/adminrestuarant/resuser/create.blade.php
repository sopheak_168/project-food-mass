@extends('adminrestuarant.master')

@section('title','Restuarant User')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-user"></i> Add User</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <form role="form" action="{{ url('/systemres/resuser') }}" method="POST">
                        {{ csrf_field() }}
                        <!-- text input -->
                        <div class="form-group">
                            <label>ResID</label>
                            <input type="text" name="" class="form-control" placeholder="Enter your ResID">
                        </div>
                        <div class="form-group">
                            <label>RUser Name</label>
                            <input type="text" name="" class="form-control" placeholder="Enter your RUser name">
                        </div>
                        <div class="form-group">
                            <label>RPassword</label>
                            <input type="password" name="" class="form-control" placeholder="Enter your RPassword">
                        </div>
                        <div class="form-group">
                            <label>Confirm RPassword</label>
                            <input type="password" name="" class="form-control" placeholder="Enter your Confirm RPassword">
                        </div>
                        <div class="box-footer">
                            <input type="submit" value="Save" class="btn btn-primary pull-right">
                        </div>
                    </form>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
@endsection