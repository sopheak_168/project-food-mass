@extends('adminrestuarant.master')

@section('title','MenuType Restuarant')

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title"><i class="fa fa-users"></i> MenuType Restuarant</h3>
            <a class="btn btn-primary pull-right" href="{{ url('/systemres/menutype/create') }}" role="button">Create New MenuType</a>
        </div>
        <div class="box-body">
            <table class="table table-bordered" id="app-table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>MenuType ID</th>
                        <th>MenuType Name</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>MT001</td>
                        <td>Test1</td>
                        <td>
                            <a href="#"><i class="fa fa-lock"></i></a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection