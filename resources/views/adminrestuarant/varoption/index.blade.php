@extends('adminrestuarant.master')

@section('title','VarOption')

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title"><i class="fa fa-users"></i> VarOption</h3>
            <a class="btn btn-primary pull-right" href="{{ url('/systemres/varoption/create') }}" role="button">Create New VarOption</a>
        </div>
        <div class="box-body">
            <table class="table table-bordered" id="app-table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>ResID</th>
                        <th>MenuID</th>
                        <th>OptionID</th>
                        <th>SubOptionID</th>
                        <th>Price</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>001</td>
                        <td>001</td>
                        <td>001</td>
                        <td>001</td>
                        <td>$0.00</td>
                        <td>
                            <a href="#"><i class="fa fa-lock"></i></a>
                        </td>
                    </tr>                    
                    <tr>
                        <td>2</td>
                        <td>001</td>
                        <td>001</td>
                        <td>001</td>
                        <td>002</td>
                        <td>$0.00</td>
                        <td>
                            <a href="#"><i class="fa fa-lock"></i></a>
                        </td>
                    </tr>                    
                    <tr>
                        <td>3</td>
                        <td>001</td>
                        <td>001</td>
                        <td>001</td>
                        <td>003</td>
                        <td>$0.00</td>
                        <td>
                            <a href="#"><i class="fa fa-lock"></i></a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection