@extends('adminrestuarant.master')

@section('title','VarSize')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-user"></i> Add VarSize</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <form role="form" action="{{ url('/systemres/varsize') }}" method="POST">
                        {{ csrf_field() }}
                        <!-- text input -->
                        <div class="form-group">
                            <label>MenuID</label>
                            <select class="form-control" name="role">
                                <option value="">--select--</option>
                                <option value="Test001">Test001</option>
                                <option value="Test002">Test002</option>
                            </select>
                        </div>                        
                        <div class="form-group">
                            <label>SizeID</label>
                            <select class="form-control" name="role">
                                <option value="">--select--</option>
                                <option value="Test001">Test001</option>
                                <option value="Test002">Test002</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Price</label>
                            <input type="text" name="" class="form-control" placeholder="Enter your Price">
                        </div>
                        <div class="box-footer">
                            <input type="submit" value="Save" class="btn btn-primary pull-right">
                        </div>
                    </form>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
@endsection