@extends('adminrestuarant.master')

@section('title','VarSize')

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title"><i class="fa fa-users"></i> VarSize</h3>
            <a class="btn btn-primary pull-right" href="{{ url('/systemres/varsize/create') }}" role="button">Create New VarSize</a>
        </div>
        <div class="box-body">
            <table class="table table-bordered" id="app-table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Menu ID</th>
                        <th>SizeID</th>
                        <th>Price</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>M001</td>
                        <td>Si001</td>
                        <td>3$</td>
                        <td>
                            <a href="#"><i class="fa fa-lock"></i></a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection