@extends('adminrestuarant.master')

@section('title','SubOption')

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title"><i class="fa fa-users"></i> SubOption</h3>
            <a class="btn btn-primary pull-right" href="{{ url('/systemres/suboption/create') }}" role="button">Create New Option</a>
        </div>
        <div class="box-body">
            <table class="table table-bordered" id="app-table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>ResID</th>
                        <th>SubOptionID</th>
                        <th>SubOptionName</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>001</td>
                        <td>001</td>
                        <td>TestSubOption1</td>
                        <td>
                            <a href="#"><i class="fa fa-lock"></i></a>
                        </td>
                    </tr>      
                    <tr>
                        <td>2</td>
                        <td>001</td>
                        <td>002</td>
                        <td>TestSubOption2</td>
                        <td>
                            <a href="#"><i class="fa fa-lock"></i></a>
                        </td>
                    </tr>  
                </tbody>
            </table>
        </div>
    </div>
@endsection