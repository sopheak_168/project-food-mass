@extends('adminrestuarant.master')

@section('title','Size')

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title"><i class="fa fa-users"></i> Size</h3>
            <a class="btn btn-primary pull-right" href="{{ url('/systemres/size/create') }}" role="button">Create New Size</a>
        </div>
        <div class="box-body">
            <table class="table table-bordered" id="app-table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>SizeID</th>
                        <th>SizeName</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>Si001</td>
                        <td>Small</td>
                        <td>
                            <a href="#"><i class="fa fa-lock"></i></a>
                        </td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>Si002</td>
                        <td>Medium</td>
                        <td>
                            <a href="#"><i class="fa fa-lock"></i></a>
                        </td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Si003</td>
                        <td>Large</td>
                        <td>
                            <a href="#"><i class="fa fa-lock"></i></a>
                        </td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>Si004</td>
                        <td>Regular</td>
                        <td>
                            <a href="#"><i class="fa fa-lock"></i></a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection