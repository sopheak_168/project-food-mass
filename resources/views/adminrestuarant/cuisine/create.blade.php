@extends('adminrestuarant.master')

@section('title','Cuisine')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-user"></i> Add Cuisine</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <form role="form" action="{{ url('/systemres/cuisine') }}" method="POST">
                        {{ csrf_field() }}
                        <!-- text input -->
                        <div class="form-group">
                            <label>CuisineID</label>
                            <input type="text" name="" class="form-control" placeholder="Enter your CuisineID">
                        </div>
                        <div class="form-group">
                            <label>CuisineID Name</label>
                            <input type="text" name="" class="form-control" placeholder="Enter your Cuisine name">
                        </div>
                        <div class="form-group">
                            <label>Description</label> 
                            <textarea class="form-control" rows="4" placeholder="Enter your Description"></textarea>
                            {{--  <input type="text" name="" class="form-control" placeholder="Enter your Description">  --}}
                        </div>
                        <div class="box-footer">
                            <input type="submit" value="Save" class="btn btn-primary pull-right">
                        </div>
                    </form>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
@endsection