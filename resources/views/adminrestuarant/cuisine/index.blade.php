@extends('adminrestuarant.master')

@section('title','Cuisine')

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title"><i class="fa fa-users"></i> Cuisine</h3>
            <a class="btn btn-primary pull-right" href="{{ url('/systemres/cuisine/create') }}" role="button">Create New Cuisine</a>
        </div>
        <div class="box-body">
            <table class="table table-bordered" id="app-table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>CuisineID</th>
                        <th>CuisineName</th>
                        <th>Description</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>C001</td>
                        <td>NameTest</td>
                        <td>DESC</td>
                        <td>
                            <a href="#"><i class="fa fa-lock"></i></a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection