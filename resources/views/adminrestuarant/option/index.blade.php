@extends('adminrestuarant.master')

@section('title','Option')

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title"><i class="fa fa-users"></i> Option</h3>
            <a class="btn btn-primary pull-right" href="{{ url('/systemres/option/create') }}" role="button">Create New Option</a>
        </div>
        <div class="box-body">
            <table class="table table-bordered" id="app-table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>ResID</th>
                        <th>OptionID</th>
                        <th>OptionName</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>001</td>
                        <td>001</td>
                        <td>TestOption1</td>
                        <td>
                            <a href="#"><i class="fa fa-lock"></i></a>
                        </td>
                    </tr>      
                    <tr>
                        <td>2</td>
                        <td>001</td>
                        <td>002</td>
                        <td>TestOption2</td>
                        <td>
                            <a href="#"><i class="fa fa-lock"></i></a>
                        </td>
                    </tr>  
                </tbody>
            </table>
        </div>
    </div>
@endsection