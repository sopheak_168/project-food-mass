<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// admin
Route::prefix('system')->group(function (){
    Route::get('/', 'Admin\LoginController@Index');
    Route::get('dashboard', 'Admin\DashboardController@Index');

    Route::get('user', 'Admin\UserController@Index');
    Route::get('user/create', 'Admin\UserController@create');
    Route::post('user', 'Admin\UserController@store');

    Route::get('staff', 'Admin\StaffController@Index');
    Route::get('staff/create', 'Admin\StaffController@create');
    Route::post('staff', 'Admin\StaffController@store');

    Route::get('role', 'Admin\RoleController@Index');
    Route::get('role/create', 'Admin\RoleController@create');
    Route::post('role', 'Admin\RoleController@store');

    Route::get('module', 'Admin\ModuleController@Index');
    Route::get('module/create', 'Admin\ModuleController@create');
    Route::post('module', 'Admin\ModuleController@store');

    Route::get('submodule', 'Admin\SubModuleController@Index');
    Route::get('submodule/create', 'Admin\SubModuleController@create');
    Route::post('submodule', 'Admin\SubModuleController@store');

    Route::get('privilege', 'Admin\PrivilegeController@Index');
    Route::get('privilege/create', 'Admin\PrivilegeController@create');
    Route::post('privilege', 'Admin\PrivilegeController@store');

    Route::get('restuarant', 'Admin\RestuarantController@Index');
    Route::get('restuarant/create', 'Admin\RestuarantController@create');
    Route::post('restuarant', 'Admin\RestuarantController@store');

});

// admin restuarant
Route::prefix('systemres')->group(function (){
    Route::get('dashboard', 'AdminRestuarant\DashboardResController@Index');

    Route::get('menu', 'AdminRestuarant\MenuController@Index');
    Route::get('menu/create', 'AdminRestuarant\MenuController@create');
    Route::post('menu', 'AdminRestuarant\MenuController@store');

    Route::get('menutype', 'AdminRestuarant\MenuTypeController@Index');
    Route::get('menutype/create', 'AdminRestuarant\MenuTypeController@create');
    Route::post('menutype', 'AdminRestuarant\MenuTypeController@store');

    Route::get('varsize', 'AdminRestuarant\VarSizeController@Index');
    Route::get('varsize/create', 'AdminRestuarant\VarSizeController@create');
    Route::post('varsize', 'AdminRestuarant\VarSizeController@store');

    Route::get('size', 'AdminRestuarant\SizeController@Index');
    Route::get('size/create', 'AdminRestuarant\SizeController@create');
    Route::post('size', 'AdminRestuarant\SizeController@store');

    Route::get('varoption', 'AdminRestuarant\VarOptionController@Index');
    Route::get('varoption/create', 'AdminRestuarant\VarOptionController@create');
    Route::post('varoption', 'AdminRestuarant\VarOptionController@store');

    Route::get('option', 'AdminRestuarant\OptionController@Index');
    Route::get('option/create', 'AdminRestuarant\OptionController@create');
    Route::post('option', 'AdminRestuarant\OptionController@store');

    Route::get('suboption', 'AdminRestuarant\SubOptionController@Index');
    Route::get('suboption/create', 'AdminRestuarant\SubOptionController@create');
    Route::post('suboption', 'AdminRestuarant\SubOptionController@store');

    Route::get('cuisine', 'AdminRestuarant\CuisineController@Index');
    Route::get('cuisine/create', 'AdminRestuarant\CuisineController@create');
    Route::post('cuisine', 'AdminRestuarant\CuisineController@store');

    Route::get('resuser', 'AdminRestuarant\ResUserController@Index');
    Route::get('resuser/create', 'AdminRestuarant\ResUserController@create');
    Route::post('resuser', 'AdminRestuarant\ResUserController@store');

});

//front
Route::prefix('home')->group(function (){
    Route::get('/', 'HomeController@Index');
    Route::get('payment', 'PaymentController@Index');
    Route::get('login', 'LoginController@Index');
    Route::get('userprofile', 'UserprofileController@Index');
    Route::get('browse', 'BrowseRestaurantController@Index');
    Route::get('menu', 'MenuRestaurantController@Index');
    Route::get('contact', 'ContactController@Index');
    Route::get('registerrest','RegisterRestController@index');
});